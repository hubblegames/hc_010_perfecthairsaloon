﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BaseAudioManager : MonoBehaviour
{

    private static BaseAudioManager _ins;

    public static BaseAudioManager Ins
    {
        get
        {
            if (_ins == null)
                _ins = FindObjectOfType<BaseAudioManager>();

            return _ins;
        }
    }
    
    public static BaseAudioManager ins;

    public Sound[] sounds;

    private void Awake()
    {
        ins = this;

        foreach (var sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;

            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
            sound.lenght = sound.clip.length;
        }
    }

    public void Play(string name)
    {
        Sound sound = Array.Find(sounds, Sound => Sound.name == name);
        sound.source.Play();
    }

    public void Stop(string name)
    {
        Sound sound = Array.Find(sounds, Sound => Sound.name == name);
        sound.source.Stop();
    }
	
    public IEnumerator DeleteAudioComponent(float waitTime, AudioSource source)
    {
        yield return new WaitForSeconds(waitTime);

        Destroy(source);
    }


}