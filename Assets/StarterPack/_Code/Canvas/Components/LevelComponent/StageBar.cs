﻿using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.Components.LevelComponent
{
    public class StageBar : MonoBehaviour
    {
        public GameObject prefStageIcon;
        public int stageCount;
        public List<GameObject> compStageIconList;
        
        void Start()
        {
        
        }

        void Update()
        {
        
        }

        void CreateStages()
        {
            foreach (var icon in compStageIconList)
                Destroy(icon);
            
            compStageIconList = new List<GameObject>();
            
            
        }
    }
}
