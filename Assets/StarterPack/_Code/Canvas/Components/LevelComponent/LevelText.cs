﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Code.StarterPack.Canvas.Components.LevelComponent
{
    public class LevelText : MonoBehaviour
    {
        [Header("Component")] public TextMeshProUGUI compCurLevel;
        public string prefix = "Level ";
        public int levelOffset;

        public void SetLevel(int level, bool starterScene)
        {
#if UNITY_EDITOR
            if (!starterScene)
            {
                Debug.Log("Not starter scene ");
                var netLevel = SceneManager.GetActiveScene().buildIndex + levelOffset - 1;
                compCurLevel.text = prefix + netLevel;
            }
            else
            {
                level += +levelOffset;
                compCurLevel.text = prefix + level;
            }
#else
            level = level + levelOffset;
            compCurLevel.text = prefix + level;
#endif
        }
    }
}