﻿using _Code.StarterPack.MainLevel;
using Code.Scripts.Manager.Canvas.Enum;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base.Panels;
using UnityEngine;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class StartGamePanel : MonoBehaviour
    {
        private LevelManager _levelManageable;
        private FixedPanel _fixedPanel;

        public Transform Go_BTN;

        private void Start()
        {
            _levelManageable = LevelManager.Ins;

            _fixedPanel = GetComponent<FixedPanel>();

            _fixedPanel.onOpenEvents.AddListener(() => OnOpen());
            _fixedPanel.onCloseEvents.AddListener(() => OnClose());
        }

        private void OnOpen()
        {
            BounceText();
        }

        private void OnClose()
        {
            //When Panel is Closed
        }

        public void BTN_StartGame()
        {
            _levelManageable.OnStart();
            _fixedPanel.OpenPanelById(CanvasId.InGame);
            _fixedPanel.OpenPanelById(CanvasId.Tutorial);
            _fixedPanel.CloseYourSelf();
        }

        private void BounceText()
        {
            DOTween.Sequence()
                .SetId(_fixedPanel.panelId)
                .Append(Go_BTN.DOScale(Vector3.one * 1.1f, 1f).SetEase(Ease.InCirc))
                .Append(Go_BTN.DOScale(Vector3.one, 1f).SetEase(Ease.OutCirc))
                .AppendCallback(BounceText);
        }


    }
}
