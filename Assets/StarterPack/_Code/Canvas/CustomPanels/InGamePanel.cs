﻿using _Code.StarterPack.Canvas.Components.LevelComponent;
using _Code.StarterPack.MainLevel;
using Plugins._StarterPack.Code.Canvas.Components.LevelComponent;
using System.Collections.Generic;
using UnityEngine;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class InGamePanel : MonoBehaviour
    {
        // --- Managers
        private LevelManager _levelManager;

        private LevelText[] _levelTexts;

        public List<GameObject> tickList = new List<GameObject>();
        public int activeTick = 0;

        public void Awake()
        {
            _levelManager = LevelManager.Ins;
            _levelTexts = GetComponentsInChildren<LevelText>();
        }

        public void UpdateTexts(int level, bool isStarterScene)
        {
            foreach (var levelText in _levelTexts)
                levelText.SetLevel(level, isStarterScene);
        }

        public void BTN_Level_Restart()
        {
            _levelManager.OnRestartLevel();
        }

        public void OpenTick()
        {
            tickList[activeTick].SetActive(true);
            activeTick++;
        }

    }
}