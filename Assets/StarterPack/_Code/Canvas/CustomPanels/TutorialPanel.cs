﻿using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base.Panels;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.CustomPanels
{
    public class TutorialPanel : MonoBehaviour
    {
        private FixedPanel _fixedPanel;

        public CanvasGroup tapAndHold;
        public CanvasGroup tapAndDrag;
        public CanvasGroup release;

        void Start()
        {
            _fixedPanel = GetComponent<FixedPanel>();

            _fixedPanel.onOpenEvents.AddListener(OnOpen);
            _fixedPanel.onCloseEvents.AddListener(OnClose);

        }

        private static void OnOpen()
        {

        }

        private static void OnClose()
        {

        }

        public void OpenTutorial(TutorialAnimationType tutorialAnimationType)
        {

            switch (tutorialAnimationType)
            {
                case TutorialAnimationType.TapAndHold:
                    tapAndHold.DOFade(1, 1f);
                    break;
                case TutorialAnimationType.TapAndDrag:
                    tapAndDrag.DOFade(1, 1f);
                    break;
                case TutorialAnimationType.Release:
                    release.DOFade(1, 1f);
                    break;
                default:
                    Debug.LogError("Open Tutorial Enum Exception");
                    break;
            }
        }

        public void CloseTutorial(TutorialAnimationType tutorialAnimationType)
        {
            switch (tutorialAnimationType)
            {
                case TutorialAnimationType.TapAndHold:
                    tapAndHold.DOFade(0, 0.5f);
                    break;
                case TutorialAnimationType.TapAndDrag:
                    tapAndDrag.DOFade(0, 0.5f);
                    break;
                case TutorialAnimationType.Release:
                    release.DOFade(0, 0.5f);
                    break;
                default:
                    Debug.LogError("Open Tutorial Enum Exception");
                    break;
            }
        }
    }
}
