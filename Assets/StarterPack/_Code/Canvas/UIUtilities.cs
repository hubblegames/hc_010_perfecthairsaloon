﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace Plugins._StarterPack.Code.Canvas.CustomPanels
{
    public static class UiUtilities
    {
        public static Tween IncreaseNumber(
            this TextMeshProUGUI txtMeshPro,
            int startValue,
            int endValue,
            float duration = 1
        )
        {
            return DOTween.To(() => startValue, x => startValue = x, endValue, duration)
                .OnUpdate(
                    () => txtMeshPro.text = startValue + "");
        }

        public static Tween FadeIn(
            this RectTransform rectTransform,
            float duration,
            CanvasGroup canvasGroup)
        {
            return DOTween.Sequence()
                .Append(canvasGroup.DOFade(1, duration));
        }

        public static Tween FadeOut(
            this RectTransform rectTransform,
            float duration,
            CanvasGroup canvasGroup)
        {
            return DOTween.Sequence()
                .Append(canvasGroup.DOFade(0, duration));
        }

        public static Tween ScaleUp(this RectTransform rectTransform, float duration = 0.3f)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOScale(Vector3.zero, 0f))
                .Append(rectTransform.DOScale(Vector3.one, 0.3f));
        }

        public static Tween ScaleDown(
            this RectTransform rectTransform,
            float duration = 0.3f)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOScale(Vector3.one, 0f))
                .Append(rectTransform.DOScale(Vector3.zero, duration));
        }

        public static Tween SlideInCenter(
            this RectTransform rectTransform,
            float duration,
            Vector2 screenCenter)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOMove(screenCenter, duration, true));
        }

        public static Tween SlideOutRight(
            this RectTransform rectTransform,
            float duration,
            Vector2 screenCenter,
            float screenWidth)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOMove(new Vector2(screenWidth + screenCenter.x, screenCenter.y), duration, true));
        }

        public static Tween SlideOutLeft(
            this RectTransform rectTransform,
            float duration,
            Vector2 screenCenter,
            float screenWidth)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOMove(new Vector2(-(screenWidth - screenCenter.x), screenCenter.y), duration, true));
        }

        public static Tween SlideOutUp(
            this RectTransform rectTransform,
            float duration,
            Vector2 screenCenter,
            float screenHeight)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOMoveY(screenHeight + screenCenter.y, duration, true));
        }

        public static Tween SlideOutDown(
            this RectTransform rectTransform,
            float duration,
            Vector2 screenCenter,
            float screenHeight)
        {
            return DOTween.Sequence()
                .Append(rectTransform.DOMoveY(-(screenHeight - screenCenter.y), duration, true));
        }
    }
}