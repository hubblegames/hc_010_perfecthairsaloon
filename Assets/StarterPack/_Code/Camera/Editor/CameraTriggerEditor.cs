﻿using System;
using System.Linq;
using _Code.StarterPack.Camera;
using Cinemachine;
using Plugins._StarterPack.Camera;
using Plugins._StarterPack.Code.Scripts.Managers.Camera;
using UnityEditor;
using UnityEngine;

namespace Code.Scripts.Manager.Camera.Editor
{
    [CustomEditor(typeof(CameraTrigger))]
    public class CameraTriggerEditor : UnityEditor.Editor
    {
        private CameraTrigger _compCameraObject;


        private static GUIStyle _toggleButtonStyleNormal = null;
        private static GUIStyle _toggleButtonStyleToggled = null;

        private int _selectedTagId;

        public override void OnInspectorGUI()
        {
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script:", MonoScript.FromMonoBehaviour((CameraTrigger) target),
                typeof(CameraTrigger), false);
            GUI.enabled = true;
            
            EditorGUILayout.LabelField("Trigger Type", EditorStyles.boldLabel);
            TriggerSettings();
            
            EditorGUILayout.Space(10);
            EditorGUILayout.LabelField("Trigger Settings", EditorStyles.boldLabel);
            TriggerValues();
        }


        private void TriggerSettings()
        {
            GUILayout.BeginHorizontal();
            if (_toggleButtonStyleNormal == null)
            {
                _toggleButtonStyleNormal = "Button";
                _toggleButtonStyleToggled = new GUIStyle(_toggleButtonStyleNormal)
                {
                    normal = {background = GUI.skin.button.active.background}
                };
            }

            GUILayout.Space(20);
            if (GUILayout.Button("Trigger With Id",
                _compCameraObject.triggerWithId ? _toggleButtonStyleToggled : _toggleButtonStyleNormal))
                _compCameraObject.triggerWithId = true;

            if (GUILayout.Button("Trigger With Key",
                !_compCameraObject.triggerWithId ? _toggleButtonStyleToggled : _toggleButtonStyleNormal))
                _compCameraObject.triggerWithId = false;

            GUILayout.EndHorizontal();

            if (_compCameraObject.triggerWithId)
            {
                _compCameraObject.cameraObject = (CameraObject) EditorGUILayout.ObjectField("Camera Object",
                    _compCameraObject.cameraObject, typeof(CameraObject), true);
            }
            else
            {
                _compCameraObject.cameraKey =
                    (CameraKey) EditorGUILayout.EnumPopup("Camera Key", _compCameraObject.cameraKey);
            }
        }

        private void TriggerValues()
        {
            _compCameraObject.delay = EditorGUILayout.FloatField("Delay", _compCameraObject.delay);

            _selectedTagId = EditorGUILayout.Popup("Tags", _selectedTagId, GetAllTags());
            _compCameraObject.triggerTag = GetAllTags()[_selectedTagId];
        }

        public void OnEnable()
        {
            _compCameraObject = (CameraTrigger) target;
            _selectedTagId = FindIndex(_compCameraObject.triggerTag);
            if (_selectedTagId == -1)
            {
                _compCameraObject.tag = "Untagged" ;
                _selectedTagId = 0;
            }
            
        }

        private static string[] GetAllTags()
        {
            return UnityEditorInternal.InternalEditorUtility.tags;
        }

        private static int FindIndex(string tag)
        {
            return GetAllTags().ToList().FindIndex(s=> s== tag);
        }
    }
}