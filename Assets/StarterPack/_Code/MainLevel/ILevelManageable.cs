﻿namespace _Code.StarterPack.MainLevel
{
    public interface ILevelCanStart
    {
        void OnStart();
    }
}
