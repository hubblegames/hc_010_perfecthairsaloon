﻿using System.Collections.Generic;
using System.Linq;
using _Code.StarterPack.Camera;
using _Code.StarterPack.Canvas.CustomPanels;
using Code.Scripts.Manager.Canvas.Enum;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Code.StarterPack.MainLevel
{
    public class LevelManager : MonoBehaviour
    {
        private static LevelManager _ins;

        public static LevelManager Ins
        {
            get
            {
                if (_ins == null)
                    _ins = FindObjectOfType<LevelManager>();
                return _ins;
            }
        }

        // --- Managers
        private BaseCanvasManager _baseCanvasManager;
        private DataManager _baseDataManager;
        private BaseLevelSelector _levelSelectable;
        private CameraManager _compCameraManager;

        // --- Level
        [SerializeField] private List<GameObject> compStartObjects;
        [SerializeField] private List<ILevelCanStart> compLevelCanStarts;

        private void Awake()
        {
            _levelSelectable = GetComponent<BaseLevelSelector>();

            _ins = this;
            _compCameraManager = CameraManager.Ins;
            compLevelCanStarts = new List<ILevelCanStart>();

            // --- Call On Start for all LevelCanStart Objects
            foreach (var canStart in compStartObjects.Select(startObject => startObject.GetComponent<ILevelCanStart>())
                .Where(canStart => canStart != null))
                compLevelCanStarts.Add(canStart);
        }

        private void Start()
        {
            _baseDataManager = DataManager.Ins;
            _baseCanvasManager = BaseCanvasManager.Ins;

            // --- Update Level Texts
            var inGamePanel = _baseCanvasManager.FindPanel<InGamePanel>();
            inGamePanel.UpdateTexts(DataManager.Level, DataManager.StarterScene);

            // --- Update Win Panel
            var compWinPanel = _baseCanvasManager.FindPanel<WinPanel>();
            compWinPanel.SetStartValues(DataManager.LevelCoin, DataManager.PrizePercentage);

            // --- Update Current Coin
            var constPanel = _baseCanvasManager.FindPanel<ConstPanel>();
            constPanel.SetStartValues(DataManager.Coin);
        }

        public void OnStart()
        {
            _compCameraManager.OpenCamera(CameraKey.Camera1);
            _baseCanvasManager.OpenPanelById(CanvasId.InGame);
            foreach (var canStart in compLevelCanStarts)
                canStart.OnStart();
        }

        public void OnLose()
        {
            DOTween.Sequence().AppendInterval(1f)
                .AppendCallback(() =>
                {
                    _baseCanvasManager.ClosePanelById(CanvasId.InGame);
                    _baseCanvasManager.OpenPanelById(CanvasId.Lose);
                });
        }

        public void OnWin()
        {
            DataManager.Coin += DataManager.LevelCoin;
            DataManager.PrizePercentage += DataManager.PrizePercentageIncrease(DataManager.Level);
            DataManager.Level++;

            _baseCanvasManager.ClosePanelById(CanvasId.InGame);

            var increase = DataManager.PrizePercentageIncrease(DataManager.Level);
            _baseCanvasManager.FindPanel<WinPanel>().OnOpen(increase, DataManager.Coin);
            _baseCanvasManager.OpenPanelById(CanvasId.Win);
        }

        public void OnPause()
        {
        }

        public void OnRestartLevel()
        {
            DOTween.KillAll();
            DOTween.Clear();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void OnNextLevel()
        {
            DOTween.KillAll();
            DOTween.Clear();
            _levelSelectable.OpenLevel(DataManager.Level);
        }
    }
}