using _Code.StarterPack.Camera;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Obstacle : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player player = other.GetComponent<Player>();

            DOTween.Sequence()
                .AppendInterval(0)
                .AppendCallback(() =>
                {
                    Debug.Log("sa");

                    CameraManager.Ins.StartShaking(1f);
                    player.speed = 1;
                })
                .AppendInterval(4f)
                .AppendCallback(() =>
                {
                    CameraManager.Ins.StopShaking();
                    player.speed = 0.2f;
                })
            ;
        }
    }
}
