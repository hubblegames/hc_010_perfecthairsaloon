using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectContainer : MonoBehaviour
{
    [Header("Components")]
    public List<Transform> effects = new List<Transform>();
    public Woman compWoman;
    public float tipAmount;

    void Start()
    {
    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            foreach (var effect in effects)
            {
                effect.SetParent(other.transform);
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            foreach (var effect in effects)
                effect.transform.localScale -= effect.transform.localScale * (0.3f * Time.deltaTime);

            if (compWoman != null)
            {
                tipAmount += 1f * Time.deltaTime;
                compWoman.hairMeshRenderer.material.SetFloat("_TipAmount", tipAmount);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            foreach (var effect in effects)
                effect.SetParent(transform);

    }
}
