using _Code.StarterPack.MainLevel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    public bool isFailed = true;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!isFailed)
                LevelManager.Ins.OnWin();
            else
                LevelManager.Ins.OnLose();
            Player player = other.GetComponent<Player>();
            player.Finish();
        }
    }
}
