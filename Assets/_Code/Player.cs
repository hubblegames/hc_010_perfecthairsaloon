using _Code.StarterPack.MainLevel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base;
using _Code.StarterPack.Canvas.CustomPanels;

public class Player : MonoBehaviour, ILevelCanStart
{
    [Header("Values")]
    public bool isActive = false;
    public float speed = 2f;
    public float downSpeed = 0.2f;

    [Header("Components")]
    public GameObject paintParticle;
    public GameObject stinkParticle;
    private InGamePanel compInGamePanel;
    public GameObject combItem;
    public GameObject paintItem;

    void Start()
    {
        compInGamePanel = SimpleCanvasManager.Ins.GetPanel<InGamePanel>();
    }

    public void OnStart()
    {
        isActive = true;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            compInGamePanel.OpenTick();
        }


        if (!isActive)
            return;

        if (Input.GetMouseButtonDown(0))
        {
            transform.DOMoveY(1, 0.3f);
            speed = downSpeed;
        }

        if (Input.GetMouseButtonUp(0))
        {
            transform.DOMoveY(2f, 0.3f);
            speed = 1f;
        }

        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    public void ColorMat(Material material)
    {
        paintItem.GetComponent<Renderer>().material = material;
    }

    public void Finish()
    {
        isActive = false;
        speed = 0;
    }

    public void OpenParticle()
    {
        paintParticle.SetActive(true);
    }

    public void CloseParticle()
    {
        paintParticle.SetActive(false);
    }

    public void OpenBrush()
    {
        combItem.SetActive(false);
        paintItem.SetActive(true);
    }

    public void OpenShit(Material mat)
    {
        stinkParticle.SetActive(true);
        paintItem.GetComponent<Renderer>().material = mat;
    }
}
