using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeItem : MonoBehaviour
{
    [Header("Prefabs")]
    public GameObject prfRainbow;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player player = other.GetComponent<Player>();
            player.OpenBrush();

            Instantiate(prfRainbow, transform.position, Quaternion.identity);
        }
    }
}
