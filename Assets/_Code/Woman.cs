using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Woman : MonoBehaviour
{
    [Header("Values")]
    public float animWeightValue = 0;

    [Header("Components")]
    public Renderer hairMeshRenderer;
    public Animator compAnimator;
    public Transform emojiPos;

    [Header("Prefabs")]
    public GameObject prfSmileEmoji;

    void Start()
    {

    }

    void Update()
    {

    }

    public void ChangeWeight(float animWeight)
    {
        compAnimator.SetLayerWeight(1, animWeight);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ChangeWeight(animWeightValue);
            Instantiate(prfSmileEmoji, emojiPos.transform.position, Quaternion.identity);
        }
    }
}
